import numpy as np
import matplotlib.pyplot as plt
import argparse

def read_file(p: str,dtype: str):
    data = np.fromfile(p,dtype)
    arr = data.reshape(21600,43200)
    return arr


def plot_data(data):
    piece_of_data = data[::50,::50]
    plt.imshow(piece_of_data)
    plt.show()

def get_user_input():
    parser = argparse.ArgumentParser(description='Get lat and long.')
    parser.add_argument('lat', type=float)
    parser.add_argument('long', type=float)
    args = parser.parse_args()
    return args.lat,args.long


def define_land_and_water(lat: float, long: float, data):
    horizontal = 21600/2
    vertical = 43200/2
    if long > 0: # x axis
       converted_long = int(horizontal + long/0.00833)
    else:
       converted_long = int(horizontal - long/0.00833)
    if lat > 0: # y axis
       converted_lat = int(vertical - lat/0.00833)
    else:
       converted_lat = int(vertical + lat/0.00833)
    print(converted_long,converted_lat)
    if data[converted_long][converted_lat] > 0:
        print("Land")
    else:
        print("Water")
    print(data[converted_long][converted_lat])
